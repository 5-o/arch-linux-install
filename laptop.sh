# Set the keyboard layout.
loadkeys uk

# Update the system clock.
timedatectl set-ntp true

# Partition the disks.
parted /dev/sda mklabel gpt mkpart ESP fat32 1MiB 513MiB \
    mkpart primary ext4 513MiB 100%
parted /dev/sda set 1 boot on

# Encrypt, format and mount the main partition.
cryptsetup -y -v luksFormat /dev/sda2
cryptsetup open /dev/sda2 cryptroot
mkfs.ext4 /dev/mapper/cryptroot
mount /dev/mapper/cryptroot /mnt

# Mount the boot partition.
mkfs.fat -F32 /mnt/boot
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

# Configure the mirrors.
readonly mirror1="mirror.bytemark.co.uk/archlinux"
readonly mirror2="archlinux.mirrors.uk2.net"
readonly mirror3="www.mirrorservice.org/sites/ftp.archlinux.org"
readonly mirror4="arch.serverspace.co.uk/arch"
readonly mirror5="mirrors.manchester.m247.com/arch-linux"
readonly before="Server = http://"
readonly after="/\$repo/os/\$arch" 

echo "$before$mirror1$after" >> tmpmirrors.txt
echo "$before$mirror2$after" >> tmpmirrors.txt
echo "$before$mirror3$after" >> tmpmirrors.txt
echo "$before$mirror4$after" >> tmpmirrors.txt
echo "$before$mirror5$after" >> tmpmirrors.txt
cat /etc/pacman.d/mirrorlist >> tmpmirrors.txt
mv tmpmirrors.txt /etc/pacman.d/mirrorlist

# Install the base packages.
pacstrap /mnt base

# Generate the Fstab file.
genfstab -U /mnt >> /mnt/etc/fstab

# Change root into the new system.
arch-chroot /mnt

# Set the time zone.
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime

# Run hwclock to generate /etc/adjtime.
hwclock --systohc

# Append localizations to /etc/locale.gen.
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen

# Generate the locales.
locale-gen

# Set the LANG variable in locale.conf.
echo "LANG=en_GB.UTF-8 UTF-8" >> /etc/locale.conf

# Make the keyboard layout permanent.
echo "KEYMAP=uk" >> /etc/vconsole.conf

# Create the hostname.
echo "trueghiassi" >> /etc/hostname
echo "127.0.1.1  trueghiassi.localdomain  trueghiassi" >> /etc/hosts

# Edit mkinitcpio file for encryption.
# New hooks line:
readonly newhooks="HOOKS=\"base udev autodetect modconf keyboard keymap block encrypt filesystems keyboard fsck\""
# Delete existing hooks line.
sed -i '/^HOOKS/d' /etc/mkinitcpio.conf 
# Put in new hooks line:
echo $newhooks >> /etc/mkinitcpio

# Recreate the initramfs image.
mkinitcpio -p linux
